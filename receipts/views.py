from django.shortcuts import render, redirect
from receipts.models import USERMODEL, Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, AccountForm, ExpenseForm


# @login_required()
# def ReceiptList(request):
#     all_receipts = Receipt.objects.all()
#     context = {"receipt": all_receipts}

#     return render(request, "receipt/list.html", context)

# @login_required()
# def ReceiptCreate(request):
#     if request.method == "POST":
#         form = Receipt(request.POST)
#         if form.is_valid():
#             form.save()
#             new = form.save(commit=False)
#             new.
#             new.save()
#             return redirect()
#     context = {"receipt": all_receipts}

#     return render(request, "receipt/create.html", context)

# login_required is to make sure the user is logged in
# before they're allowed to access the view it decorates
@login_required()
def ReceiptList(request):
    all_receipts = Receipt.objects.filter(purchaser=request.user)
    context = {"receipt": all_receipts}
    return render(request, "receipt/list.html", context)


@login_required()
def ReceiptCreate(request):
    # This is what happens if the user fills out the form, and
    # presses the button 'create'
    # the request, is a method to 'post' the data
    if request.method == "POST":
        # the form for the data is stored here
        form = ReceiptForm(request.POST)
        # if the form is filled out correctly by the user
        if form.is_valid():
            # the form is saved but not commited to database
            item = form.save(commit=False)
            # the form.purchaser is then set to the user who made the request
            # that user is the same user that is logged in
            item.purchaser = request.user
            # the item is then saved to the database
            item.save()
            # the user is redirected to 'home'
            return redirect("home")
    # otherwise, if there is no request to post
    else:
        # the form is shown as setup in forms.py
        form = ReceiptForm()
        # adding this here will filter the ACCOUNT field dropdown menu
        # to only show those used/created by the current user
        form.fields["account"].queryset = Account.objects.filter(
            owner=request.user
        )
        # adding this here will filter the CATEGORY field dropdown menu
        # to only show those used/created by the current user
        form.fields["category"].queryset = ExpenseCategory.objects.filter(
            owner=request.user
        )
    # the context is the form itself
    context = {"form": form}
    # returns the request type to receipt/create.html with the context
    return render(request, "receipt/create.html", context)


@login_required()
def ExpenseList(request):
    category = ExpenseCategory.objects.filter(owner=request.user)
    context = {"expensecategory": category}
    return render(request, "categories/list.html", context)


@login_required()
def ExpenseCreate(request):
    if request.method == "POST":
        form = ExpenseForm(request.POST)
        if form.is_valid():
            form.instance.owner = request.user
            form.save()
            return redirect("list_categories")
            # item = form.save(commit=False)
            # item.owner = request.user
            # item.save()
            # return redirect("list_categories")
    else:
        form = ExpenseForm()
    context = {"form": form}
    return render(request, "categories/create.html", context)


@login_required()
def AccountList(request):
    category = Account.objects.filter(owner=request.user)
    context = {"account": category}
    return render(request, "accounts/list.html", context)


@login_required()
def AccountCreate(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            item = form.save(commit=False)
            item.owner = request.user
            item.save()
            return redirect("list_account")
    else:
        form = AccountForm()
    context = {"form": form}
    return render(request, "accounts/create.html", context)
