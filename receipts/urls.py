from django.urls import path
from receipts.views import ReceiptList, ReceiptCreate
from receipts.views import ExpenseList, ExpenseCreate
from receipts.views import AccountList, AccountCreate


urlpatterns = [
    path("", ReceiptList, name="home"),
    path("create/", ReceiptCreate, name="create_receipt"),
    path("categories/", ExpenseList, name="list_categories"),
    path("categories/create/", ExpenseCreate, name="create_category"),
    path("accounts/", AccountList, name="list_account"),
    path("accounts/create/", AccountCreate, name="create_account"),
]
